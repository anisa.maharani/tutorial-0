package id.ac.ui.cs.advprog.tutorial0.model;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class Course {
    private String id;
    private String name;
    private Boolean vacancyStatus = true;
}
